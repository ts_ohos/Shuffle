/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.meetic.shuffle;

import ohos.agp.animation.Animator;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.utils.Point;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

/**
 * ShuffleListener
 */
public class ShuffleListener implements Component.TouchEventListener {
  private static final int INVALID_POINTER_ID = -1;

  private final float mObjectX;
  private final float mObjectY;
  private final int mObjectH;
  private final int mObjectW;
  private final int mParentWidth;
  private final int mParentHeight;
  private final Listener mListener;
  private final Object mDataObject;
  private final float mHalfWidth;
  private float BASE_ROTATION_DEGREES;

  private float mPosX;
  private float mPosY;
  private float mDownTouchX;
  private float mDownTouchY;

  // The active pointer is the one currently moving our object.
  private int mActivePointerId = INVALID_POINTER_ID;
  private Component mFrame = null;

  private final int TOUCH_ABOVE = 0;
  private final int TOUCH_BELOW = 1;
  private int mTouchPosition;
  private final Object obj = new Object();
  private boolean mIsAnimationRunning = false;
  private float MAX_COS = (float) Math.cos(Math.toRadians(45));
  private int slideType = 0;
  private final int SLIDETYPE_ACEOSS = 1;
  private final int SLIDETYPE_lVERTICAL = 2;
  private boolean isSlide = true;
  private boolean imageSpin = true;
  private int rightDuration;
  private int leftDuration;

  public ShuffleListener(
      ComponentContainer parent, int slideType, Component frame, boolean imageSpin,
      Object itemAtPosition, Listener listener) {
    this(parent, slideType, frame, imageSpin, itemAtPosition, 15f, listener);
  }

  public ShuffleListener(
      ComponentContainer parent, int slideType,
      Component frame, boolean imageSpin,
      Object itemAtPosition,
      float rotation_degrees,
      Listener listener) {
    super();
    this.mFrame = frame;
    this.slideType = slideType;
    this.mObjectX = frame.getContentPositionX();
    this.mObjectY = frame.getContentPositionY();
    this.mObjectH = frame.getHeight();
    this.mObjectW = frame.getWidth();
    this.mHalfWidth = mObjectW / 2f;
    this.imageSpin = imageSpin;
    this.mDataObject = itemAtPosition;
    this.mParentWidth = parent.getWidth();
    this.mParentHeight = parent.getHeight();
    this.BASE_ROTATION_DEGREES = rotation_degrees;
    this.mListener = listener;
  }

  public void setCannotSlide(boolean isSlide) {
    this.isSlide = isSlide;
  }

  @Override
  public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
    MmiPoint point = touchEvent.getPointerScreenPosition(0);
    if (isSlide) {
      switch (touchEvent.getAction()) {
        case TouchEvent.PRIMARY_POINT_DOWN:
          float downTouchX = point.getX();
          float downTouchY = point.getY();
          mDownTouchX = downTouchX;
          mDownTouchY = downTouchY;

          // to prevent an initial jump of the magnifier, aposX and aPosY must
          // have the values from the magnifier frame
          if (mPosX == 0) {
            mPosX = mFrame.getContentPositionX();
          }
          if (mPosY == 0) {
            mPosY = mFrame.getContentPositionY();
          }

          if (downTouchY - mFrame.getContentPositionY() < mObjectH / 2f) {
            mTouchPosition = TOUCH_ABOVE;
          } else {
            mTouchPosition = TOUCH_BELOW;
          }
          break;
        case TouchEvent.PRIMARY_POINT_UP:
          mActivePointerId = INVALID_POINTER_ID;
          resetShuffleViewOnStack();
          break;
        case TouchEvent.OTHER_POINT_DOWN:
          break;
        case TouchEvent.OTHER_POINT_UP:
          break;
        case TouchEvent.POINT_MOVE:
          if (slideType == SLIDETYPE_ACEOSS) {
            final float moveTouchX = point.getX();

            // Calculate the distance moved
            final float dx = moveTouchX - mDownTouchX;

            // Move the frame
            mPosX += dx;

            // calculate the rotation degrees
            float distobjectX = mPosX - mObjectX;

            float rotation = BASE_ROTATION_DEGREES * 2.f * distobjectX / mParentWidth;

            // move the component
//            mFrame.setTranslationX(mFrame.getContentPositionX() + dx);
            mFrame.setPosition((int) (mFrame.getContentPositionX() + dx), (int) mFrame.getContentPositionY());
            mFrame.setRotation(imageSpin ? rotation : 0);
            mListener.onScroll(getScrollProgressPercent());

            // save the touch point position
            mDownTouchX = moveTouchX;
          } else if (slideType == SLIDETYPE_lVERTICAL) {
            final float moveTouchY = point.getY();

            // Calculate the distance moved
            final float dy = moveTouchY - mDownTouchY;

            // Move the frame
            mPosY += dy;

            // calculate the rotation degrees
            float distobjectX = mPosX - mObjectX;

            float rotation = BASE_ROTATION_DEGREES * 2.f * distobjectX / mParentWidth;

            // move the component
//            mFrame.setTranslationY(mFrame.getContentPositionY() + dy);
            mFrame.setPosition((int) (mFrame.getContentPositionX()), (int) (mFrame.getContentPositionY() + dy));
            mFrame.setRotation(imageSpin ? rotation : 0);
            mListener.onScroll(getScrollProgressPercent());

            // save the touch point position
            mDownTouchY = moveTouchY;
          } else {
            final float moveTouchX = point.getX();
            final float moveTouchY = point.getY();

            // Calculate the distance moved
            final float dx = moveTouchX - mDownTouchX;
            final float dy = moveTouchY - mDownTouchY;

            // Move the frame
            mPosX += dx;
            mPosY += dy;

            // calculate the rotation degrees
            float distobjectX = mPosX - mObjectX;

            float rotation = BASE_ROTATION_DEGREES * 2.f * distobjectX / mParentWidth;

            // move the component
//            mFrame.setTranslationX(mFrame.getContentPositionX() + dx);
//            mFrame.setTranslationY(mFrame.getContentPositionY() + dy);
            mFrame.setPosition((int) (mFrame.getContentPositionX() + dx), (int) (mFrame.getContentPositionY() + dy));
            mFrame.setRotation(imageSpin ? rotation : 0);
            mListener.onScroll(getScrollProgressPercent());

            // save the touch point position
            mDownTouchX = moveTouchX;
            mDownTouchY = moveTouchY;
          }
          break;
        case TouchEvent.CANCEL:
          mActivePointerId = INVALID_POINTER_ID;
          break;
      }
    }
    return true;
  }

  private float getScrollProgressPercent() {
    if (movedBeyondLeftBorder()) {
      return -1f;
    } else if (movedBeyondRightBorder()) {
      return 1f;
    } else {
      float zeroToOneValue = (mPosX + mHalfWidth - leftBorder()) / (rightBorder() - leftBorder());
      return zeroToOneValue * 2f - 1f;
    }
  }

  private boolean resetShuffleViewOnStack() {
    if (slideType == SLIDETYPE_lVERTICAL) {
      if (movedBeyondTopBorder()) {
        onSelected(false, getExitPoint(mObjectH), 5);
      } else if (movedBeyondBottomBorder()) {
        onSelected(false, getExitPoint(-mObjectH), 5);
      } else {
        float abslMoveDistance = Math.abs(mPosX - mObjectX);
        mPosX = 0;
        mPosY = 0;
        mDownTouchX = 0;
        mDownTouchY = 0;
        mFrame.createAnimatorProperty()
            .setDuration(200)
            .setCurveType(Animator.CurveType.OVERSHOOT)
            .moveToX(mObjectX)
            .moveToY(mObjectY)
            .rotate(0)
            .start();
        mListener.onScroll(0.0f);
        if (abslMoveDistance < 4.0) {
          mListener.onClick(mDataObject);
        }
      }
    } else {
      if (movedBeyondLeftBorder()) {
        // Left shuffle
        onSelected(true, getExitPoint(-mObjectW), 100);
        mListener.onScroll(-1.0f);
      } else if (movedBeyondRightBorder()) {
        // Right shuffle
        onSelected(false, getExitPoint(mParentWidth), 100);
        mListener.onScroll(1.0f);
      } else {
        float abslMoveDistance = Math.abs(mPosX - mObjectX);
        mPosX = 0;
        mPosY = 0;
        mDownTouchX = 0;
        mDownTouchY = 0;
        mFrame.createAnimatorProperty()
            .setDuration(200)
            .setCurveType(Animator.CurveType.OVERSHOOT)
            .moveToX(mObjectX)
            .moveToY(mObjectY)
            .rotate(0)
            .start();
        mListener.onScroll(0.0f);
        if (abslMoveDistance < 4.0) {
          mListener.onClick(mDataObject);
        }
      }
    }
    return false;
  }

  private boolean movedBeyondLeftBorder() {
    return mPosX + mHalfWidth < leftBorder();
  }

  private boolean movedBeyondTopBorder() {
    return mPosY + mObjectH / 2f < TopBorder();
  }

  private boolean movedBeyondRightBorder() {
    return mPosX + mHalfWidth > rightBorder();
  }

  private boolean movedBeyondBottomBorder() {
    return mPosY +  mObjectH / 2f > BottomBorder();
  }


  public void shuffleRight(int duration) {
    this.rightDuration = duration;
  }

  public void shuffleLeft(int duration) {
    this.leftDuration = duration;
  }

  public int getCurrentAdapterPosition() {
    return mTouchPosition;
  }

  public float leftBorder() {
    return mParentWidth / 4.f;
  }

  public float TopBorder() {
    return mParentHeight / 4.f;
  }

  public float rightBorder() {
    return 3 * mParentWidth / 4.f;
  }

  public float BottomBorder() {
    return 3 * mParentHeight / 4.f;
  }

  public void onSelected(final boolean isLeft, float exitY, long duration) {
    mIsAnimationRunning = true;
    float exitX;
    if (!isLeft) {
      exitX = mParentWidth + getRotationWidthOffset();
    } else {
      exitX = -mObjectW - getRotationWidthOffset();
    }

    this.mFrame
        .createAnimatorProperty()
        .setDuration(duration)
        .setCurveType(Animator.CurveType.ACCELERATE)
        .moveByX(exitX)
        .moveByY(exitY)
        .setStateChangedListener(
            new Animator.StateChangedListener() {
              @Override
              public void onStart(Animator animator) {
              }

              @Override
              public void onStop(Animator animator) {
              }

              @Override
              public void onCancel(Animator animator) {
              }

              @Override
              public void onEnd(Animator animator) {
                if (!isLeft) {
                  mListener.onViewExited();
                  mListener.leftExit(mDataObject);
                } else {
                  mListener.onViewExited();
                  mListener.rightExit(mDataObject);
                }
                mIsAnimationRunning = false;
              }

              @Override
              public void onPause(Animator animator) {
              }

              @Override
              public void onResume(Animator animator) {
              }
            })
        .rotate(getExitRotation(!isLeft))
        .start();
  }

  /**
   * Starts a default left exit animation.
   */
  public void selectLeft() {
    if (!mIsAnimationRunning) {
      onSelected(true, mObjectY, 1000);
    }
  }

  /**
   * Starts a default right exit animation.
   */
  public void selectRight() {
    if (!mIsAnimationRunning) {
      onSelected(false, mObjectY, 1000);
    }
  }

  private float getExitPoint(int exitXPoint) {
    float[] x = new float[2];
    x[0] = mObjectX;
    x[1] = mPosX;

    float[] y = new float[2];
    y[0] = mObjectY;
    y[1] = mPosY;

    ShuffleSettings regression = new ShuffleSettings(x, y);

    // Your typical y = ax+b linear regression
    return (float) regression.slope() * exitXPoint + (float) regression.intercept();
  }

  private float getExitRotation(boolean isLeft) {
    float rotation = BASE_ROTATION_DEGREES * 2.f * (mParentWidth - mObjectX) / mParentWidth;
    if (mTouchPosition == TOUCH_BELOW) {
      rotation = -rotation;
    }
    if (isLeft) {
      rotation = -rotation;
    }
    return rotation;
  }

  /**
   * When the object rotates it's width becomes bigger.
   * The maximum width is at 45 degrees.
   * <p/>
   * The below method calculates the width offset of the rotation.
   *
   * @return rotation width offset
   */
  private float getRotationWidthOffset() {
    return mObjectW / MAX_COS - mObjectW;
  }

  public void setRotationDegrees(float degrees) {
    this.BASE_ROTATION_DEGREES = degrees;
  }

  public boolean isTouching() {
    return this.mActivePointerId != INVALID_POINTER_ID;
  }

  public Point getLastPoint() {
    return new Point(this.mPosX, this.mPosY);
  }

  protected interface Listener {
    void onViewExited();

    void leftExit(Object dataObject);

    void rightExit(Object dataObject);

    void onClick(Object dataObject);

    void onScroll(float scrollProgressPercent);
  }
}
