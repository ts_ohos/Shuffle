/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.meetic.shuffle;

import ohos.agp.components.AttrSet;
import ohos.app.Context;

/**
 * BaseFlingAdapterView
 */
abstract class BaseShuffleAdapterView extends ShuffleProviderComponent {
  private int heightMeasureSpec;

  private int widthMeasureSpec;

  public BaseShuffleAdapterView(Context context) {
    super(context);
  }

  public BaseShuffleAdapterView(Context context, AttrSet attrSet) {
    super(context, attrSet);
  }

  public BaseShuffleAdapterView(Context context, AttrSet attrSet, String styleName) {
    super(context, attrSet, styleName);
  }

  public int getWidthMeasureSpec() {
    return widthMeasureSpec;
  }

  public int getHeightMeasureSpec() {
    return heightMeasureSpec;
  }

}
