package com.meetic.shuffle;

import java.util.HashSet;
import java.util.Set;
import ohos.agp.components.*;
import ohos.agp.database.DataSetSubscriber;
import ohos.agp.render.Canvas;
import ohos.agp.utils.Point;
import ohos.app.Context;

/**
 * Shuffle
 */
public class Shuffle extends BaseShuffleAdapterView {
  private int MIN_ADAPTER_STACK = DEFAULT_MAX_VISIBLE;
  private float ROTATION_DEGREES = 15.f;

  private BaseItemProvider mAdapter;
  private int LAST_OBJECT_IN_STACK = 0;
  private Listener listener;
  private AdapterDataSetObserver mDataSetObserver;
  private boolean mInLayout = false;
  private Component mActiveShuffle = null;
  private OnItemClickListener mOnItemClickListener;
  private ShuffleListener shuffleListener;
  private ShuffleProviderComponent shuffleProviderComponent;
  private Point mLastTouchPoint;
  private int slideType = 0;
  Set<Listener> listeners = new HashSet<>();
  // 图片倾斜
  private boolean imageSpin = true;
  private boolean isShuffleOverlap = false;

  public Shuffle(Context context) {
    this(context, null);
  }

  public Shuffle(Context context, AttrSet attrSet) {
    this(context, attrSet, null);
  }

  public Shuffle(Context context, AttrSet attrSet, String styleName) {
    super(context, attrSet, styleName);
  }

  public void SlideType(int slideType) {
    this.slideType = slideType;
  }

  public void setImageSpin(boolean imageSpin) {
    this.imageSpin = imageSpin;
  }

  public void setIsShuffleOverlap(boolean isShuffleOverlap) {
    this.isShuffleOverlap = isShuffleOverlap;
    setShuffleOverlap(isShuffleOverlap);
  }

  /**
   * A shortcut method to set both the listeners and the adapter.
   *
   * @param context  The activity context which extends onFlingListener, OnItemClickListener or
   *                both
   * @param mAdapter The adapter you have to set.
   */
  public void init(final Context context, BaseItemProvider mAdapter) {
    if (context instanceof Listener) {
      listener = (Listener) context;
    } else {
      throw new RuntimeException("ability does not implement Shuffle" +
          ".Listener");
    }
    if (context instanceof OnItemClickListener) {
      mOnItemClickListener = (OnItemClickListener) context;
    }
    setItemProvider(mAdapter);
  }

  @Override
  public Component getSelectedComponent() {
    return mActiveShuffle;
  }

  @Override
  public void postLayout() {
    if (!mInLayout) {
      super.postLayout();
    }
  }

  @Override
  public void onRefreshed(Component component) {
    // if we don't have an adapter, we don't need to do anything
    if (mAdapter == null) {
      return;
    }
    mInLayout = true;
    Component topShuffle = getComponentAt(LAST_OBJECT_IN_STACK);
    if (mActiveShuffle != null && topShuffle != null && topShuffle == mActiveShuffle) {
      if (this.shuffleListener.isTouching()) {
        Point lastPoint = this.shuffleListener.getLastPoint();
        if (this.mLastTouchPoint == null || !this.mLastTouchPoint.equals(lastPoint)) {
          this.mLastTouchPoint = lastPoint;
          removeComponents(0, LAST_OBJECT_IN_STACK);
        }
      }
    } else {
      setTopView();
    }
    mInLayout = false;
  }

  @Override
  public void onDraw(Component component, Canvas canvas) {
    final int adapterCount = mAdapter.getCount();
    if (adapterCount <= MIN_ADAPTER_STACK) {
      listener.onAdapterAboutToEmpty(adapterCount);
    }
  }

  /**
   * Set the top view and add the fling listener
   */
  private void setTopView() {
    if (getChildCount() > 0) {
      mActiveShuffle = getComponentAt(LAST_OBJECT_IN_STACK);
      if (mActiveShuffle != null) {
        shuffleListener =
            new ShuffleListener(
                this,
                slideType, mActiveShuffle, imageSpin ? true : false,
                mAdapter.getItem(0),
                ROTATION_DEGREES,
                new ShuffleListener.Listener() {
                  @Override
                  public void onViewExited() {
                    mActiveShuffle = null;
                    listener.removeFirstObjectInAdapter();
                  }

                  @Override
                  public void leftExit(Object dataObject) {
                    listener.onLeftExit(dataObject);
                  }

                  @Override
                  public void rightExit(Object dataObject) {
                    listener.onRightExit(dataObject);
                  }

                  @Override
                  public void onClick(Object dataObject) {
                    if (mOnItemClickListener != null) {
                      mOnItemClickListener.onItemClicked(0, dataObject);
                    }
                  }

                  @Override
                  public void onScroll(float scrollProgressPercent) {
                    listener.onScroll(scrollProgressPercent);
                  }
                });
        mActiveShuffle.setTouchEventListener(shuffleListener);
      }
    }
  }
  public void addListener(Listener listener) {
    listeners.add(listener);
  }

  public void removeListener(Listener listener) {
    listeners.remove(listener);
  }

  public ShuffleListener getTopShuffleListener() throws NullPointerException {
    if (shuffleListener == null) {
      throw new NullPointerException();
    }
    return shuffleListener;
  }

  public ShuffleProviderComponent getVesselListener() throws NullPointerException {
    if (shuffleProviderComponent == null) {
      throw new NullPointerException();
    }
    return shuffleProviderComponent;
  }

  public void setMinStackInAdapter(int MIN_ADAPTER_STACK) {
    this.MIN_ADAPTER_STACK = MIN_ADAPTER_STACK;
  }

  @Override
  public BaseItemProvider getItemProvider() {
    return mAdapter;
  }

  @Override
  public void setItemProvider(BaseItemProvider provider) {
    if (mAdapter != null && mDataSetObserver != null) {
      mAdapter.removeDataSubscriber(mDataSetObserver);
      mDataSetObserver = null;
    }
    mAdapter = provider;
    if (mAdapter != null && mDataSetObserver == null) {
      mDataSetObserver = new AdapterDataSetObserver();
      mAdapter.addDataSubscriber(mDataSetObserver);
    }
    super.setItemProvider(provider);
  }

  public void setListener(Listener Listener) {
    this.listener = Listener;
  }

  public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
    this.mOnItemClickListener = onItemClickListener;
  }

  @Override
  public LayoutConfig createLayoutConfig(Context context, AttrSet attrSet) {
    return new StackLayout.LayoutConfig(context, attrSet);
  }

  /**
   * The observer
   */
  private class AdapterDataSetObserver extends DataSetSubscriber {
    @Override
    public void onChanged() {
      postLayout();
    }

    @Override
    public void onInvalidated() {
      postLayout();
    }
  }

  public interface OnItemClickListener {
    void onItemClicked(int itemPosition, Object dataObject);
  }

  public interface Listener {
    void removeFirstObjectInAdapter();

    void onLeftExit(Object dataObject);

    void onRightExit(Object dataObject);

    void onAdapterAboutToEmpty(int itemsInAdapter);

    void onScroll(float scrollProgressPercent);
  }
}
