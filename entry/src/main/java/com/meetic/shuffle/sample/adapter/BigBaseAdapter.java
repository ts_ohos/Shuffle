package com.meetic.shuffle.sample.adapter;

import com.meetic.shuffle.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

public class BigBaseAdapter extends BaseItemProvider {
  private Context mContext;

  private int[] mDataList;

  /**
   * constructor
   *
   * @param context context
   */
  public BigBaseAdapter(Context context) {
    mContext = context;
  }

  /**
   * set data in the list
   *
   * @param dataList data source
   */
  public void setData(int[] dataList) {
    this.mDataList = dataList;
    super.notifyDataChanged();
  }

  @Override
  public int getCount() {
    return mDataList == null ? 0 : mDataList.length;
  }

  @Override
  public Integer getItem(int position) {
    return mDataList[position];
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public Component getComponent(int position, Component convertView, ComponentContainer container) {
    MyComponentHolder holder;
    if (convertView == null) {
      convertView = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_big_crad_item,
          container, false);
      holder = new MyComponentHolder(convertView);
      convertView.setTag(holder);
    } else {
      holder = (MyComponentHolder) convertView.getTag();
    }
    Image text = (Image) holder.getItemView().findComponentById(ResourceTable.Id_big_image);
//        text.setText(mDataList.get(position));
    text.setImageAndDecodeBounds(mDataList[position]);
    return convertView;
  }

  /**
   * item component holder
   */
  public static class MyComponentHolder {
    private final Component itemView;

    public MyComponentHolder(Component itemView) {
      if (itemView == null) {
        throw new IllegalArgumentException("itemView may not be null");
      } else {
        this.itemView = itemView;
      }
    }

    public Component getItemView() {
      return itemView;
    }
  }
}
