package com.meetic.shuffle.sample;

import com.meetic.shuffle.Shuffle;
import com.meetic.shuffle.sample.adapter.BaseAdapter;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

import java.util.Arrays;

public class HorizontalAbility extends AbilitySlice implements Component.ClickedListener,
    Shuffle.Listener {

  private Shuffle shuffleContainer;

  private BaseAdapter mProvider;

  private Button btnLeft, btnRight, btnSlideLeft, btnSlideRight, btnRestart, btnRevert;

  private int[] icon = new int[]{
      ResourceTable.Media_girl,
      ResourceTable.Media_girl,
      ResourceTable.Media_girl,
      ResourceTable.Media_girl,
      ResourceTable.Media_girl,
      ResourceTable.Media_girl,
      ResourceTable.Media_girl,
  };

  private int mCount;
  private int type;

  private final int TYPE_CANNOT = -1;
  private final int TYPE_DEFAULT = 0;
  private final int TYPE_ONE = 1;
  private final int TYPE_TWO = 2;
  private final int TYPE_THREE = 3;

  @Override
  public void onStart(Intent intent) {
    super.onStart(intent);
    super.setUIContent(ResourceTable.Layout_ability_horizontal);
    shuffleContainer = (Shuffle) findComponentById(ResourceTable.Id_frame);

    btnLeft = (Button) findComponentById(ResourceTable.Id_left);
    btnLeft.setClickedListener(this);

    btnRight = (Button) findComponentById(ResourceTable.Id_right);
    btnRight.setClickedListener(this);

    btnSlideLeft = (Button) findComponentById(ResourceTable.Id_forbid_slide_left);
    btnSlideLeft.setClickedListener(this);

    btnSlideRight = (Button) findComponentById(ResourceTable.Id_forbid_slide_right);
    btnSlideRight.setClickedListener(this);

    btnRestart = (Button) findComponentById(ResourceTable.Id_restart);
    btnRestart.setClickedListener(this);

    btnRevert = (Button) findComponentById(ResourceTable.Id_revert);
    btnRevert.setClickedListener(this);

    shuffleContainer.setClickedListener(this);

    type = intent.getIntParam("DATA", 0);


    mProvider = new BaseAdapter(this);
    mProvider.setData(icon);
    shuffleContainer.setItemProvider(mProvider);
    shuffleContainer.setListener(this);

    switch (type) {
      case 1:
        setSlideType(TYPE_ONE, Component.HIDE);
        shuffleContainer.setImageSpin(true);
        shuffleContainer.setIsShuffleOverlap(false);
        btnSlideLeft.setVisibility(Component.HIDE);
        btnSlideRight.setVisibility(Component.HIDE);
        btnRestart.setVisibility(Component.HIDE);
        btnRevert.setVisibility(Component.HIDE);
        break;
      case 2:
        setSlideType(TYPE_TWO, Component.HIDE);
        shuffleContainer.setImageSpin(true);
        shuffleContainer.setIsShuffleOverlap(false);
        btnSlideLeft.setVisibility(Component.HIDE);
        btnSlideRight.setVisibility(Component.HIDE);
        btnRestart.setVisibility(Component.HIDE);
        btnRevert.setVisibility(Component.HIDE);
        break;
      case 3:
        shuffleContainer.SlideType(TYPE_DEFAULT);
        btnLeft.setVisibility(Component.HIDE);
        btnRight.setVisibility(Component.HIDE);
        btnSlideLeft.setVisibility(Component.VISIBLE);
        btnSlideRight.setVisibility(Component.VISIBLE);
        btnRestart.setVisibility(Component.HIDE);
        shuffleContainer.setImageSpin(true);
        shuffleContainer.setIsShuffleOverlap(false);
        btnRevert.setVisibility(Component.HIDE);
        break;
      case 4:
        setSlideType(TYPE_ONE, Component.HIDE);
        shuffleContainer.setImageSpin(false);
        shuffleContainer.setIsShuffleOverlap(false);
        btnSlideLeft.setVisibility(Component.HIDE);
        btnSlideRight.setVisibility(Component.HIDE);
        btnRestart.setVisibility(Component.HIDE);
        btnRevert.setVisibility(Component.HIDE);
        break;
      case 5:
        shuffleContainer.SlideType(TYPE_ONE);
        btnLeft.setVisibility(Component.HIDE);
        btnRight.setVisibility(Component.HIDE);
        btnRestart.setVisibility(Component.HIDE);
        shuffleContainer.setImageSpin(true);
        shuffleContainer.setIsShuffleOverlap(true);
        btnRevert.setVisibility(Component.HIDE);
        break;
      case 6:
        setSlideType(TYPE_DEFAULT, Component.HIDE);
        btnLeft.setVisibility(Component.HIDE);
        btnRight.setVisibility(Component.HIDE);
        btnSlideLeft.setVisibility(Component.HIDE);
        btnSlideRight.setVisibility(Component.HIDE);
        btnRestart.setVisibility(Component.VISIBLE);
        btnRevert.setVisibility(Component.HIDE);
        break;
      case 7:
        setSlideType(TYPE_DEFAULT, Component.HIDE);
        btnLeft.setVisibility(Component.HIDE);
        btnRight.setVisibility(Component.HIDE);
        btnSlideLeft.setVisibility(Component.HIDE);
        btnSlideRight.setVisibility(Component.HIDE);
        btnRestart.setVisibility(Component.HIDE);
        btnRevert.setVisibility(Component.VISIBLE);
        break;
      default:
        setSlideType(TYPE_DEFAULT, Component.VISIBLE);
        btnSlideLeft.setVisibility(Component.HIDE);
        btnSlideRight.setVisibility(Component.HIDE);
        btnRestart.setVisibility(Component.HIDE);
        shuffleContainer.setImageSpin(true);
        shuffleContainer.setIsShuffleOverlap(false);
        btnRevert.setVisibility(Component.HIDE);
        break;
    }
  }

  /**
   * setSlideType: 设置滑动类型。
   *
   * @param i    滑动类型
   * @param hide 显示底部按钮
   */
  private void setSlideType(int i, int hide) {
    shuffleContainer.SlideType(i);
    btnLeft.setVisibility(hide);
    btnRight.setVisibility(hide);
  }

  @Override
  public void removeFirstObjectInAdapter() {
    // this is the simplest way to delete an object from the Adapter (/AdapterView)
    LogUtil.debug("LIST", "removed object!");
//        icon.remove(0);
    mProvider.notifyDataSetItemRemoved(0);
  }

  @Override
  public void onLeftExit(Object dataObject) {
    // Do something on the left!
    // You also have access to the original object.
    // If you want to use it just cast it (String) dataObject
    makeToast(HorizontalAbility.this, "Left!");
  }

  @Override
  public void onRightExit(Object dataObject) {
    makeToast(HorizontalAbility.this, "Right!");
  }

  @Override
  public void onAdapterAboutToEmpty(int itemsInAdapter) {
    // Ask for more data here
//        al.add("XML ".concat(String.valueOf(mCount)));
//        mProvider.notifyDataSetItemInserted(al.size() - 1);
//        LogUtil.debug("LIST", "notified");
//        mCount++;
  }

  @Override
  public void onScroll(float scrollProgressPercent) {
    LogUtil.error("onScroll", "scrollProgressPercent: " + scrollProgressPercent);
    Component component = shuffleContainer.getSelectedComponent();
    component
        .findComponentById(ResourceTable.Id_item_shuffle_right_indicator)
        .setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
    component
        .findComponentById(ResourceTable.Id_surname_left)
        .setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
    component
        .findComponentById(ResourceTable.Id_item_shuffle_left_indicator)
        .setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
    component
        .findComponentById(ResourceTable.Id_surname_right)
        .setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
  }

  @Override
  public void onClick(Component component) {
    Component component1 = shuffleContainer.getSelectedComponent();

    switch (component.getId()) {
      case ResourceTable.Id_left:
        shuffleContainer.getTopShuffleListener().selectLeft();
        try {
          component1
              .findComponentById(ResourceTable.Id_item_shuffle_right_indicator)
              .setAlpha(1);
          component1
              .findComponentById(ResourceTable.Id_surname_left)
              .setAlpha(0.2f);
        } catch (Exception e) {
          e.printStackTrace();
        }
        break;
      case ResourceTable.Id_right:
        shuffleContainer.getTopShuffleListener().selectRight();
        try {
          component1
              .findComponentById(ResourceTable.Id_item_shuffle_left_indicator)
              .setAlpha(1);
          component1
              .findComponentById(ResourceTable.Id_surname_right)
              .setAlpha(0.2f);
        } catch (Exception e) {
          e.printStackTrace();
        }
        break;
      case ResourceTable.Id_frame:
        makeToast(this, "Clicked!");
        break;
      case ResourceTable.Id_forbid_slide_left:
        shuffleContainer.getTopShuffleListener().setCannotSlide(true);
        break;
      case ResourceTable.Id_forbid_slide_right:
        shuffleContainer.getTopShuffleListener().setCannotSlide(false);
        break;
      case ResourceTable.Id_restart:
        if (mProvider != null) {
          mProvider.setData(null);
        }
        mProvider.setData(icon);
        shuffleContainer.setItemProvider(mProvider);
        break;
      case ResourceTable.Id_revert:
        icon = Arrays.copyOf(icon, icon.length + 1);
        icon[icon.length -1] = ResourceTable.Media_girl;
        mProvider.setData(icon);
        shuffleContainer.setItemProvider(mProvider);
        break;
    }
  }

  private void makeToast(Context context, String text) {
    ToastDialog toast = new ToastDialog(context);
    toast.setText(text);
    // 显示Toast会导致快速滑动删除卡顿
    // toast.show();
  }
}
