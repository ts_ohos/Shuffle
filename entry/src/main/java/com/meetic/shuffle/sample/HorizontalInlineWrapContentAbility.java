package com.meetic.shuffle.sample;

import com.meetic.shuffle.Shuffle;
import com.meetic.shuffle.sample.adapter.BigBaseAdapter;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

public class HorizontalInlineWrapContentAbility extends AbilitySlice implements
    Component.ClickedListener, Shuffle.Listener {
  private Shuffle shuffle;

  private BigBaseAdapter mProvider;

  private int[] icon = new int[]{
      ResourceTable.Media_girl,
      ResourceTable.Media_girl,
      ResourceTable.Media_girl,
      ResourceTable.Media_girl,
      ResourceTable.Media_girl,
      ResourceTable.Media_girl,
      ResourceTable.Media_girl,
  };

  private int mCount;


  @Override
  public void onStart(Intent intent) {
    super.onStart(intent);
    super.setUIContent(ResourceTable.Layout_ability_horizontal_inline_without_rotation);
    shuffle = (Shuffle) findComponentById(ResourceTable.Id_frame);

    shuffle.setClickedListener(this);


    mProvider = new BigBaseAdapter(this);
    mProvider.setData(icon);
    shuffle.setItemProvider(mProvider);
    shuffle.setListener(this);
  }

  @Override
  public void removeFirstObjectInAdapter() {
    // this is the simplest way to delete an object from the Adapter (/AdapterView)
    LogUtil.debug("LIST", "removed object!");
//        icon.remove(0);
    mProvider.notifyDataSetItemRemoved(0);
  }

  @Override
  public void onLeftExit(Object dataObject) {
    // Do something on the left!
    // You also have access to the original object.
    // If you want to use it just cast it (String) dataObject
    makeToast(HorizontalInlineWrapContentAbility.this, "Left!");
  }

  @Override
  public void onRightExit(Object dataObject) {
    makeToast(HorizontalInlineWrapContentAbility.this, "Right!");
  }

  @Override
  public void onAdapterAboutToEmpty(int itemsInAdapter) {
    // Ask for more data here
//        al.add("XML ".concat(String.valueOf(mCount)));
//        mProvider.notifyDataSetItemInserted(al.size() - 1);
//        LogUtil.debug("LIST", "notified");
//        mCount++;
  }

  @Override
  public void onScroll(float scrollProgressPercent) {

  }


  private void makeToast(Context context, String text) {
    ToastDialog toast = new ToastDialog(context);
    toast.setText(text);
    // 显示Toast会导致快速滑动删除卡顿
    // toast.show();
  }

  @Override
  public void onClick(Component component) {

  }
}
