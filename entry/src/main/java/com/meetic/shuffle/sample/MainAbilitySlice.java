/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.meetic.shuffle.sample;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;

/**
 * 程序主界面
 */
public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {

  private AbilitySlice slice;
  private Intent intent;

  @Override
  public void onStart(Intent intent) {
    super.onStart(intent);
    super.setUIContent(ResourceTable.Layout_ability_main);


    Button horizontal = (Button) findComponentById(ResourceTable.Id_horizontal);
    Button horizontalInline = (Button) findComponentById(ResourceTable.Id_horizontalInline);
    Button horizontalInlineBehind =
        (Button) findComponentById(ResourceTable.Id_horizontalInlineBehind);
    Button horizon = (Button) findComponentById(ResourceTable.Id_horizontalInlineWithoutRotation);
    Button vertical = (Button) findComponentById(ResourceTable.Id_vertical);
    Button enableDisable = (Button) findComponentById(ResourceTable.Id_enableDisable);
    Button stackChange = (Button) findComponentById(ResourceTable.Id_stackChange);
    Button wrapContent = (Button) findComponentById(ResourceTable.Id_wrapContent);
    Button restart = (Button) findComponentById(ResourceTable.Id_restart);
    Button revert = (Button) findComponentById(ResourceTable.Id_revert);

    horizontal.setClickedListener(this);
    horizontalInline.setClickedListener(this);
    horizontalInlineBehind.setClickedListener(this);
    horizon.setClickedListener(this);
    vertical.setClickedListener(this);
    enableDisable.setClickedListener(this);
    stackChange.setClickedListener(this);
    wrapContent.setClickedListener(this);
    restart.setClickedListener(this);
    revert.setClickedListener(this);
  }

  @Override
  public void onClick(Component component) {
    switch (component.getId()) {
      case ResourceTable.Id_horizontal:
        skipBasicePage(0);
        break;
      case ResourceTable.Id_horizontalInline:
        skipBasicePage(1);
        break;
      case ResourceTable.Id_horizontalInlineBehind:
        skipBasicePage(1);
        break;
      case ResourceTable.Id_horizontalInlineWithoutRotation:
        skipBasicePage(4);
        break;
      case ResourceTable.Id_vertical:
        skipBasicePage(2);
        break;
      case ResourceTable.Id_enableDisable:
        skipBasicePage(3);
        break;
      case ResourceTable.Id_stackChange:
        skipBasicePage(5);
        break;
      case ResourceTable.Id_wrapContent:
        slice = new HorizontalInlineWrapContentAbility();
        intent = new Intent();
        present(slice, intent);
        break;
      case ResourceTable.Id_restart:
        skipBasicePage(6);
        break;
      case ResourceTable.Id_revert:
        skipBasicePage(7);
        break;
    }
  }

  private void skipBasicePage(int i) {
    slice = new HorizontalAbility();
    intent = new Intent();
    intent.setParam("DATA", i);
    present(slice, intent);
  }
}
