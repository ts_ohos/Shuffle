# Shuffle

本项目是基于开源项目Shuffle进行harmonyos化的移植和开发的，可以通过项目标签以及
[github地址](https://github.com/Meetic/Shuffle)

移植版本：源master v1.0.4 版本

## 项目介绍
### 项目名称：Shuffle
### 所属系列：harmonyos的第三方组件适配移植
### 功能：
    自定义横扫布局。
    已支持部分：
    1、Shuffle滑动监听
    2、左滑设定距离删除Shuffle，不满足设定距离Shuffle回弹；
    3、右滑设定距离删除Shuffle，不满足设定距离Shuffle回弹；
    4、上滑Shuffle回弹；
    5、下滑Shuffle回弹；
    6、Shuffle点击监听；
    7、Shuffle左滑到达设定距离监听
    8、Shuffle右滑到达设定距离监听
    9、Shuffle少于设定数量监听

### 项目移植状态：完全移植
### 调用差异：基本没有使用差异，请参照demo使用
### 原项目Doc地址：https://github.com/Meetic/Shuffle
### 编程语言：java

### 项目截图（涉及文件仅供demo测试使用）

![demo运行效果](art/Y.png)
![运行效果](art/P.gif)

## 安装教程

#### 方案一  
 
可以先下载项目，将项目中的shuffle库提取出来放在所需项目中通过build配置
```Java
dependencies {
     implementation project(":shuffle")
}
```

#### 方案二

- 1.项目根目录的build.gradle中的repositories添加：
```groovy
    buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
```
- 2.开发者在自己的项目中添加依赖
```groovy
dependencies {
    implementation 'com.gitee.ts_ohos:shuffle:1.0.0'
}
```

# How to use
    <com.meetic.shuffle.Shuffle
            ohos:id="$+id:frame"
            ohos:width="match_parent"
            ohos:height="match_parent"
            ohos:background_element="$graphic:background_ability_main"/>

Ability中添加：

     shuffle = (Shuffle) findComponentById(ResourceTable.Id_frame);
     mProvider = new BaseAdapter(this);
     mProvider.setData(al);
     shuffle.setItemProvider(mProvider);
     
     核心接口：
     
       SlideType();
       滑动类型（0：全局滑动 1：左右滑动 2： 上下滑动）

       setImageSpin()：
       设置图片旋转；
     
       setIsShuffleOverlap(boolean isShuffleOverlap)：
       设置Shuffle重叠方式(ture上重叠 / false 下重叠)；

       shuffle.getTopShuffleListener().setCannotSlide(true);
       设置是可否滑动（true 可滑动 / false 不可）
       
#License

    Copyright 2016 Meetic, Inc.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
