# Changelog
本组件基于原库master v1.0.4 版本移植

### v.1.0.0
移植OHOS后首次上传

support
自定义横扫布局, 基本没有使用差异。

not support
无

1.0.8

- infinite

1.0.6

- revert

1.0.4

- Layer animations 

1.0.3

- ShuffleViewAnimator is now customisable

1.0.2

- added restart shuffling